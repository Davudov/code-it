package com.davud.codeit;

import android.os.Bundle;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.davud.codeit.di.DI;

import toothpick.Scope;
import toothpick.Toothpick;


public class BaseActivity extends MvpAppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Scope scope = Toothpick.openScope(DI.APP);
        Toothpick.inject(this, scope);
    }

}
