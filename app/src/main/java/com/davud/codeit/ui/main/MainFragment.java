package com.davud.codeit.ui.main;

import com.davud.codeit.BaseFragment;
import com.davud.codeit.R;

public class MainFragment extends BaseFragment {
    @Override
    public int getLayout() {
        return R.layout.fragment_main;
    }
}
