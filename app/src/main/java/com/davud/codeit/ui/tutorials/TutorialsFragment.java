package com.davud.codeit.ui.tutorials;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.davud.codeit.BaseFragment;

public class TutorialsFragment extends BaseFragment implements TutorialsView {
    @InjectPresenter
    TutorialsPresenter tutorialsPresenter;

    @ProvidePresenter
    TutorialsPresenter tutorialsPresenter() {
        return new TutorialsPresenter();
    }

    @Override
    public int getLayout() {
        return 0;
    }


}
