package com.davud.codeit.ui.practise;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.davud.codeit.BaseFragment;
import com.davud.codeit.R;
import com.davud.codeit.di.controllers.MenuController;

import javax.inject.Inject;

import butterknife.BindView;
import ru.terrakok.cicerone.NavigatorHolder;

public class PractiseFragment extends BaseFragment implements PractiseView {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @Inject
    MenuController menuController;
    @InjectPresenter
    PractisePresenter practisePresenter;
    @Inject
    NavigatorHolder navigatorHolder;

    @ProvidePresenter
    PractisePresenter testPresenter() {
        return new PractisePresenter();
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_practise;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.inflateMenu(R.menu.main_menu);
        toolbar.setNavigationOnClickListener(t -> menuController.open());
        toolbar.setTitle(R.string.navigation_practise);
    }

}
