package com.davud.codeit.ui.navigation;

import com.davud.codeit.BaseFragment;
import com.davud.codeit.R;

public class NavigationFragment extends BaseFragment {
    @Override
    public int getLayout() {
        return R.layout.fragment_navigation;
    }
}
