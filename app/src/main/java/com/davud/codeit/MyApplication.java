package com.davud.codeit;

import android.app.Application;

import com.davud.codeit.di.DI;
import com.davud.codeit.di.module.MenuRelayModule;
import com.davud.codeit.di.module.NavigationModule;

import toothpick.Scope;
import toothpick.Toothpick;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    void init() {
        initToothpick();
    }

    public void initToothpick() {
        Scope scope = Toothpick.openScope(DI.APP);
        scope.installModules(new NavigationModule());
        scope.installModules(new MenuRelayModule());
    }

}
