package com.davud.codeit;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.davud.codeit.di.DI;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import toothpick.Scope;
import toothpick.Toothpick;

public abstract class BaseFragment extends Fragment {
    Unbinder unbinder = Unbinder.EMPTY;

    @LayoutRes
    public abstract int getLayout();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout(), container, false);
        unbinder = ButterKnife.bind(this, view);
        Scope scope = Toothpick.openScope(DI.APP);
        Toothpick.inject(this, scope);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
