package com.davud.codeit.di.provider;

import com.davud.codeit.di.controllers.MenuController;
import com.jakewharton.rxrelay2.BehaviorRelay;

import javax.inject.Provider;

public class MenuRelayProvider implements Provider<MenuController> {
    private BehaviorRelay<Boolean> relay = BehaviorRelay.createDefault(false);

    @Override
    public MenuController get() {
        return new MenuController(relay);
    }
}
