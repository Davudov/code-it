package com.davud.codeit.di.provider;

import com.davud.codeit.MyApplication;

import javax.inject.Provider;

public class AppProvider implements Provider<MyApplication> {
    private MyApplication myApplication;

    public AppProvider(MyApplication myApplication) {
        this.myApplication = myApplication;
    }

    @Override
    public MyApplication get() {
        return myApplication;
    }
}
