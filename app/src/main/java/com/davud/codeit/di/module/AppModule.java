package com.davud.codeit.di.module;

import com.davud.codeit.MyApplication;
import com.davud.codeit.di.provider.AppProvider;

import toothpick.config.Module;

public class AppModule extends Module {
    public AppModule(MyApplication myApplication) {
        bind(MyApplication.class).toProviderInstance(new AppProvider(myApplication));
    }
}
