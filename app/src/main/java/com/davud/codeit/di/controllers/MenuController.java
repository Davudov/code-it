package com.davud.codeit.di.controllers;

import com.jakewharton.rxrelay2.BehaviorRelay;

public class MenuController {
    public BehaviorRelay<Boolean> relay;

    public MenuController(BehaviorRelay<Boolean> relay) {
        this.relay = relay;
    }

    public void open() {
        relay.accept(true);
    }
}
