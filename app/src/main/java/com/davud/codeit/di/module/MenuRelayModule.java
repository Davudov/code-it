package com.davud.codeit.di.module;

import com.davud.codeit.di.controllers.MenuController;
import com.davud.codeit.di.provider.MenuRelayProvider;

import toothpick.config.Module;

public class MenuRelayModule extends Module {
    public MenuRelayModule() {
        bind(MenuController.class).toProviderInstance(new MenuRelayProvider());
    }
}
