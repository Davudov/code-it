package com.davud.codeit.di.provider;

import javax.inject.Provider;

import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;


public class NavigationProvider implements Provider<Router> {
    private static Cicerone<Router> cicerone;

    public NavigationProvider() {
        cicerone = Cicerone.create();
    }

    public static NavigatorHolder getNavigationHolder() {
        return cicerone.getNavigatorHolder();
    }

    @Override
    public Router get() {
        return cicerone.getRouter();
    }
}