package com.davud.codeit;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.widget.Toast;

import com.davud.codeit.di.controllers.MenuController;
import com.davud.codeit.ui.main.MainFragment;
import com.davud.codeit.ui.practise.PractiseFragment;
import com.davud.codeit.ui.tutorials.TutorialsFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.android.SupportFragmentNavigator;

public class CodeItActivity extends BaseActivity {
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @Inject
    Router router;
    @Inject
    NavigatorHolder navigatorHolder;
    @Inject
    MenuController menuController;
    CompositeDisposable disposable = new CompositeDisposable();
    Navigator navigator = new SupportFragmentNavigator(getSupportFragmentManager(), R.id.placeholder) {
        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            return getFragment(screenKey, data);
        }

        @Override
        protected void showSystemMessage(String message) {
            Toast.makeText(CodeItActivity.this, message, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void exit() {

        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        router.navigateTo(Screens.PRACTISE_FRAG);
        disposable.add(menuController.relay
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::showDrawer));
    }

    private void showDrawer(Boolean openDrawer) {
        if (openDrawer)
            mDrawerLayout.openDrawer(GravityCompat.START);
        else mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    private void updateDrawer() {
        Fragment fragment = getCurrentFragment();
        boolean isAvailable = isDrawerAvailable(fragment);
        enableDrawer(isAvailable);
    }

    private Boolean isDrawerAvailable(Fragment fragment) {
        if (fragment instanceof PractiseFragment) {
            return true;
        } else return fragment instanceof TutorialsFragment;
    }

    private void enableDrawer(boolean available) {
        if (available)
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, GravityCompat.START);
        else
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.START);
    }

    private Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.placeholder);
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        super.onPause();
        navigatorHolder.removeNavigator();
    }

    Fragment getFragment(String screen, Object data) {
        switch (screen) {
            case Screens.TEST_FRAG:
                return new PractiseFragment();
            case Screens.MAIN_FRAG:
                return new MainFragment();
            case Screens.PRACTISE_FRAG:
                return new PractiseFragment();
        }

        return new MainFragment();
    }

}
